package scheduler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cloudwatchevents.AmazonCloudWatchEvents;
import com.amazonaws.services.cloudwatchevents.AmazonCloudWatchEventsClientBuilder;
import com.amazonaws.services.cloudwatchevents.model.DeleteRuleRequest;
import com.amazonaws.services.cloudwatchevents.model.EcsParameters;
import com.amazonaws.services.cloudwatchevents.model.ListRulesRequest;
import com.amazonaws.services.cloudwatchevents.model.ListTargetsByRuleRequest;
import com.amazonaws.services.cloudwatchevents.model.PutRuleRequest;
import com.amazonaws.services.cloudwatchevents.model.PutRuleResult;
import com.amazonaws.services.cloudwatchevents.model.PutTargetsRequest;
import com.amazonaws.services.cloudwatchevents.model.PutTargetsResult;
import com.amazonaws.services.cloudwatchevents.model.RemoveTargetsRequest;
import com.amazonaws.services.cloudwatchevents.model.Rule;
import com.amazonaws.services.cloudwatchevents.model.RuleState;
import com.amazonaws.services.cloudwatchevents.model.Target;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.DescribeSecurityGroupsRequest;
import com.amazonaws.services.ec2.model.DescribeSubnetsRequest;
import com.amazonaws.services.ec2.model.Filter;
import com.amazonaws.services.ec2.model.SecurityGroup;
import com.amazonaws.services.ec2.model.Subnet;
import com.amazonaws.services.ec2.model.Vpc;
import com.amazonaws.services.ecs.AmazonECS;
import com.amazonaws.services.ecs.AmazonECSClientBuilder;
import com.amazonaws.services.ecs.model.AwsVpcConfiguration;
import com.amazonaws.services.ecs.model.ContainerOverride;
import com.amazonaws.services.ecs.model.KeyValuePair;
import com.amazonaws.services.ecs.model.LaunchType;
import com.amazonaws.services.ecs.model.NetworkConfiguration;
import com.amazonaws.services.ecs.model.RunTaskRequest;
import com.amazonaws.services.ecs.model.RunTaskResult;
import com.amazonaws.services.ecs.model.TaskOverride;

@SpringBootApplication
public class Scheduler implements CommandLineRunner
{
    private static Logger LOG = LoggerFactory.getLogger(Scheduler.class);

    public static void main(String[] args)
    {
        LOG.info("SCHEDULER STARTED");
        SpringApplication.run(Scheduler.class, args);
        LOG.info("SCHEDULER EXITED");
    }

    /**
     * Simple Command line input. Providing options for:
     * - Create ECS Schedule
     * - Create individual ECS task
     * - Define how many Schedules to run
     * - Manage ECS Schedules
     * - Stress test the limits of ECS
     *
     * Also affects the ECS task removal limits.
     * @param args
     */
    @Override
    public void run(String... args)
    {
        Scanner command = new Scanner(System.in);

        boolean running = true;

        AmazonCloudWatchEvents cwClient =
                AmazonCloudWatchEventsClientBuilder.standard()
                        .withRegion(Regions.EU_WEST_1)
                        .withCredentials(new ProfileCredentialsProvider("default"))
                        .build();

        AmazonECS ecsClient = AmazonECSClientBuilder.standard()
                .withRegion(Regions.EU_WEST_1)
                .withCredentials(new ProfileCredentialsProvider("default"))
                .build();

        AmazonEC2 ec2Client = AmazonEC2ClientBuilder.standard()
                .withRegion(Regions.EU_WEST_1)
                .withCredentials(new ProfileCredentialsProvider("default"))
                .build();

        while(running){
            System.out.println("Enter command: ");

            switch(command.nextLine()){

                case "create task":
                    createTask(command, ecsClient);
                    continue;

                case "create schedule":
                    createSchedule(command, cwClient, ecsClient, ec2Client);
                    continue;

                case "manage schedules":
                    manageSchedules(command, cwClient, ecsClient);
                    continue;

                case "ecs stress test":
                    ecsStressTest(command, cwClient, ecsClient);
                    continue;

                case "help":
                    helpOptions();
                    continue;

                case "exit":
                    System.out.println("Application Closed");
                    running = false;
                    break;

                default:
                    System.out.println("Command not recognized!");
                    continue;
            }
        }
        command.close();
    }

    private void createTask(Scanner command, AmazonECS ecsClient)
    {
        String clusterInput = selectECSCluster(ecsClient, command);

        String taskDefinitionInput = selectTaskDefinition(ecsClient, command);

        // Define the subnet and security to deploy to
        AwsVpcConfiguration vpcConfiguration = new AwsVpcConfiguration();
        vpcConfiguration.setAssignPublicIp("ENABLED");
        vpcConfiguration.setSecurityGroups(Arrays.asList("sg-01b125d09f9a07b7b"));
        vpcConfiguration.setSubnets(Arrays.asList("subnet-a4852fec"));
        NetworkConfiguration networkConfiguration = new NetworkConfiguration().withAwsvpcConfiguration(vpcConfiguration);

        //Define environment variables to override
        List<KeyValuePair> environmentOverrides = new ArrayList<>();
        KeyValuePair environmentVariable = new KeyValuePair();
        environmentVariable.setName("ECS_ENGINE_TASK_CLEANUP_WAIT_DURATION");
        environmentVariable.setValue("2m");
        environmentVariable.setName("MTLN_TEST");
        environmentVariable.setValue("TRUE");
        environmentOverrides.add(environmentVariable);
        ContainerOverride containerOverride = new ContainerOverride().withName("dockerjavaapp").withEnvironment(environmentOverrides);
        TaskOverride taskOverride = new TaskOverride().withContainerOverrides(containerOverride);

        // Define ECS schedule Task
        RunTaskRequest runTaskRequest = new RunTaskRequest()
                                                .withLaunchType(LaunchType.FARGATE)
                                                .withPlatformVersion("1.3.0")
                                                .withCluster(clusterInput)
                                                .withTaskDefinition(taskDefinitionInput)
                                                .withOverrides(taskOverride)
                                                .withNetworkConfiguration(networkConfiguration)
                                                .withCount(1);

        System.out.println("DEPLOYING TASK DEFINITION");

        try{
            RunTaskResult taskResult = ecsClient.runTask(runTaskRequest);

            taskResult.getTasks().get(0).getTaskArn();

            System.out.println(taskResult.toString());

            if (taskResult.getFailures().size()!=0)
                System.out.println(taskResult.getFailures().size() + " TASK DEFINTIONS FAILED TO BE DEPLOYED");

            if (taskResult.getTasks().size()!=0)
                System.out.println(taskResult.getTasks().size() + " TASK DEFINTIONS SUCCESSFULLY DEPLOYED");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void createSchedule(Scanner command, AmazonCloudWatchEvents cwClient, AmazonECS ecsClient, AmazonEC2 ec2Client)
    {
        String clusterInput = selectECSCluster(ecsClient, command);
        String selectedTaskDefinitionARN = selectTaskDefinition(ecsClient, command);

        System.out.println("Enter schedule frequency in minutes: ");
        String scheduleFrequency = command.next();

        com.amazonaws.services.cloudwatchevents.model.NetworkConfiguration networkConfiguration = selectNetworkConfiguration(ec2Client, command);


        EcsParameters ecsParameters = selectECSParameters(selectedTaskDefinitionARN, networkConfiguration,command);

        System.out.println("\n CREATING SCHEDULE FOR ECS TASK");
        PutRuleRequest ruleRequest = new PutRuleRequest()
                .withName("fargateSchedule")
                .withScheduleExpression("rate(" + scheduleFrequency + " minutes)")
                .withState(RuleState.ENABLED);

        PutRuleResult ruleResponse = cwClient.putRule(ruleRequest);

        String ruleArn = ruleResponse.getRuleArn();

        String overriddenVariables = setEnvironmentOverrides();

        Target target = new Target()
                .withId("scheduleFargate")
                .withRoleArn("arn:aws:iam::458549032908:role/ecsEventsRole")
                .withArn(clusterInput)
                .withEcsParameters(ecsParameters)
                .withInput(overriddenVariables);

        PutTargetsRequest request = new PutTargetsRequest()
                .withTargets(target)
                .withRule("fargateSchedule");

        try {
            PutTargetsResult response = cwClient.putTargets(request);
            if (response.getFailedEntryCount()>0)
            {
                System.out.println(response.getFailedEntryCount() + "failed to deploy ECS schedule");
                return;
            }
            System.out.println("Successfully deployed ECS schedule");
            return;
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
    }

    /**
     * Allows the User to launch as many schedules as they want with varying frequency.
     */
    private void ecsStressTest(Scanner command, AmazonCloudWatchEvents cwClient, AmazonECS ecsClient)
    {
        String clusterInput = selectECSCluster(ecsClient, command);

        String taskDefinitionInput = selectTaskDefinition(ecsClient, command);
        String selectedTaskDefinitionARN = taskDefinitionInput;

        System.out.println("Enter schedule frequency in minutes: ");
        String scheduleFrequency = command.next();

        com.amazonaws.services.cloudwatchevents.model.AwsVpcConfiguration vpcConfiguration = new com.amazonaws.services.cloudwatchevents.model.AwsVpcConfiguration();
        vpcConfiguration.setAssignPublicIp("ENABLED");
        vpcConfiguration.setSecurityGroups(Arrays.asList("sg-0cbb1bfec3372cbdb"));
        vpcConfiguration.setSubnets(Arrays.asList("subnet-0d10bd9e156902e8a", "subnet-0ca735e6a3278506d","subnet-05df28b97530ed1f8"));
        com.amazonaws.services.cloudwatchevents.model.NetworkConfiguration networkConfiguration = new com.amazonaws.services.cloudwatchevents.model.NetworkConfiguration().withAwsvpcConfiguration(vpcConfiguration);

        EcsParameters ecsParameters = new EcsParameters();
        ecsParameters.setLaunchType("FARGATE");
        ecsParameters.setTaskCount(1);
        ecsParameters.setTaskDefinitionArn(selectedTaskDefinitionARN);
        ecsParameters.setPlatformVersion("1.3.0");
        ecsParameters.withNetworkConfiguration(networkConfiguration);

        System.out.println("Enter number of schedules to create: ");
        int scheduleCount = command.nextInt();

        for (int i=0; i<scheduleCount; i++)
        {
            System.out.println("\n CREATING SCHEDULE FOR ECS TASK: " + i);
            PutRuleRequest ruleRequest = new PutRuleRequest()
                    .withName("fargateSchedule" + i)
                    .withScheduleExpression("rate(" + scheduleFrequency + " minutes)")
                    .withState(RuleState.ENABLED);

            PutRuleResult ruleResponse = cwClient.putRule(ruleRequest);

            String ruleArn = ruleResponse.getRuleArn();

            String overriddenVariables = setEnvironmentOverrides();

            Target target = new Target()
                    .withId("scheduleFargate")
                    .withRoleArn("arn:aws:iam::458549032908:role/ecsEventsRole")
                    .withArn(clusterInput)
                    .withEcsParameters(ecsParameters);

            PutTargetsRequest request = new PutTargetsRequest()
                    .withTargets(target)
                    .withRule("fargateSchedule" + i);

            try {
                PutTargetsResult response = cwClient.putTargets(request);
                if (response.getFailedEntryCount()>0)
                {
                    System.out.println(response.getFailedEntryCount() + "failed to deploy ECS schedule");
                }
                System.out.println("Successfully deployed ECS schedule: " + i);
            } catch (Exception exception) {
                System.out.println(exception.getMessage());
            }
        }
    }

    private void manageSchedules(Scanner command, AmazonCloudWatchEvents cwClient, AmazonECS ecsClient)
    {
        System.out.println("Input rule prefix to delete:");
        String rulePrefix  = command.next();

        List<Rule> rules = cwClient.listRules(new ListRulesRequest().withNamePrefix(rulePrefix)).getRules();
        int i = 0;
        for (Rule rule : rules)
        {
            try {
                List<Target> targets = cwClient.listTargetsByRule(new ListTargetsByRuleRequest().withRule(rule.getName())).getTargets();
                cwClient.removeTargets(new RemoveTargetsRequest().withIds(targets.get(0).getId()).withRule(rule.getName()));
                cwClient.deleteRule(new DeleteRuleRequest().withName(rule.getName()));
                i++;
            } catch (Exception exception) {
                System.out.println(exception.getMessage());
            }
        }
        System.out.println(i + " RULES DELETED");
    }

    private String selectECSCluster(AmazonECS ecsClient, Scanner command)
    {
        int i = 0;
        List<String> clusters = ecsClient.listClusters().getClusterArns();
        for (String clusterArn : clusters)
        {
            System.out.println(i + ": " + clusterArn);
            i++;
        }
        System.out.println("Select Cluster ARN by entering index: ");
        return clusters.get(command.nextInt());
    }

    private String selectTaskDefinition(AmazonECS ecsClient, Scanner command)
    {
        int i = 0;
        List<String> taskDefs = ecsClient.listTaskDefinitions().getTaskDefinitionArns();
        for (String taskDefinition : taskDefs)
        {
            System.out.println(i + ": " + taskDefinition);
            i++;
        }
        System.out.println("Select Task Definition ARN by index: ");
        return taskDefs.get(command.nextInt());
    }

    private com.amazonaws.services.cloudwatchevents.model.NetworkConfiguration selectNetworkConfiguration(AmazonEC2 ec2Client, Scanner command)
    {
        try {
            String ECS_VPC = Optional.ofNullable(System.getenv("ECS_VPC")).orElseThrow(
                    () -> new Exception("ECS_VPC is not set in the environment"));
            Vpc ECSVPC = ec2Client.describeVpcs()
                                    .getVpcs()
                                    .stream()
                                    .filter(e -> e.getVpcId().equals(ECS_VPC))
                                    .findFirst().orElseThrow(() -> new Exception("ECS VPC could not be found in VPC list"));

            List<Subnet> subnets = ec2Client.describeSubnets(new DescribeSubnetsRequest()
                    .withFilters(new Filter("vpc-id", Arrays.asList(ECSVPC.getVpcId())))).getSubnets();

            String subnet = selectSubnet(subnets, command);


            List<SecurityGroup> securityGroups = ec2Client.describeSecurityGroups(new DescribeSecurityGroupsRequest()
                    .withFilters(new Filter("vpc-id", Arrays.asList(ECSVPC.getVpcId())))).getSecurityGroups();

            String securityGroup = selectSecurityGroup(securityGroups, command);

            System.out.println("Do you wish to assign a Public IP to the instance? ENABLED - DISABLED");
            String publicIP = command.next();


            com.amazonaws.services.cloudwatchevents.model.AwsVpcConfiguration vpcConfiguration = new com.amazonaws.services.cloudwatchevents.model.AwsVpcConfiguration();
            vpcConfiguration.setAssignPublicIp(publicIP);
            vpcConfiguration.setSecurityGroups(Arrays.asList(securityGroup));
            vpcConfiguration.setSubnets(Arrays.asList(subnet));
            com.amazonaws.services.cloudwatchevents.model.NetworkConfiguration networkConfiguration = new com.amazonaws.services.cloudwatchevents.model.NetworkConfiguration().withAwsvpcConfiguration(vpcConfiguration);

            return networkConfiguration;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
            return null;
    }

    private String selectSecurityGroup(List<SecurityGroup> securityGroups, Scanner command)
    {
        try
        {
            int i = 0;
            for (SecurityGroup securityGroup : securityGroups)
            {
                System.out.println(i + ": " + securityGroup.getGroupName());
                i++;
            }
            System.out.println("Select Security Group by index: ");
            return securityGroups.get(command.nextInt()).getGroupId();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return "";
    }

    private String selectSubnet(List<Subnet> subnets, Scanner command)
    {
        try
        {
            int i = 0;
            for (Subnet subnet : subnets)
            {
                System.out.println(i + ": " + subnet.getSubnetId());
                i++;
            }
            System.out.println("Select Subnet by index: ");
            return subnets.get(command.nextInt()).getSubnetId();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return "";
    }

    private EcsParameters selectECSParameters(String taskDef, com.amazonaws.services.cloudwatchevents.model.NetworkConfiguration networkConfig, Scanner command)
    {
        EcsParameters ecsParameters = new EcsParameters();
        ecsParameters.setLaunchType("FARGATE");
        ecsParameters.setTaskCount(1);
        ecsParameters.setTaskDefinitionArn(taskDef);
        ecsParameters.setPlatformVersion("1.3.0");
        ecsParameters.withNetworkConfiguration(networkConfig);
        return ecsParameters;
    }

    private String setEnvironmentOverrides()
    {
        Collection<JSONObject> environmentVariables = Arrays.asList(new JSONObject().put("name", "MTLN_TEST").put("value", "TRUE"),
                new JSONObject().put("name", "ECS_ENGINE_TASK_CLEANUP_WAIT_DURATION").put("value", "1m"));

        String overriddenVariables = new JSONObject()
                .put("containerOverrides", new JSONArray(Arrays.asList(new JSONObject()
                        .put("name", "dockerjavaapp")
                        .put("environment", new JSONArray(environmentVariables))))).toString();

        return overriddenVariables;
    }

    private void helpOptions()
    {
        StringBuilder sb = new StringBuilder("Command options: \n");
        sb.append("create schedule - creates scheduled ECS task\n");
        sb.append("create task - creates individual ECS task\n");
        sb.append("manage schedules - displays list of ECS schedules which can be removed\n");
        sb.append("ecs stress test - creates a large number (>50) of schedules for limit testing ECS\n");
        sb.append("help - show available commands to run\n");
        sb.append("exit - close program\n");

        System.out.println(sb.toString());
    }
}
